===================================
Simulation of Database Privacy Risk
===================================


Applying simulation methods to the task of determining database privacy risk.


* Free software: GNU General Public License v3
* Documentation (coming soon): https://privacysim.readthedocs.io.


Features
--------

* Extensible munging of datasets

* Estimates successful reidentification with matching attacks under a variety
  of assumptions about the attacker and the released data

* Runs all combinations of parameter settings from values specified for each
  individual parameter

* Distributed execution on high-performance computing clusters


