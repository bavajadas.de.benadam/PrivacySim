#!/bin/env python
"""
Performs exact matching attacks without estimating individual
reidentification risk
"""
import itertools
import os
import pickle
import random
import logging
import sys
from textwrap import dedent

import pandas as pd

import definitions
from simulator import UC_Munger, Simulator
from utilities import DatasetName


def get_all_arguments():
    research_samples = (0.25, 0.5, 1.0)
    auxiliary_samples = (0.01, 0.1, 0.2)
    auxiliary_missings = (0.0, 0.1, 0.25, 0.75)
    auxiliary_noises = (0.0, 0.1, 0.2)
    payload_varses = (['satii_writ', 'sati_verbal', 'sati_math'], ['year'],
                      ['gpa_cum'], ['race'], ['bachelor'])
    columns_to_keeps = ([
                            'year', 'race', 'state', 'sati_verbal',
                            'sati_math', 'satii_writ',
                            'ucadj_hsgpa', 'parent_income', 'ca_api_rank',
                            'gpa_cum',
                            'grad_cipcat', 'enr_campus', 'bachelor',
                            'ttd_qtrs00'
                        ],)  # note necessary trailing comma
    # number_of_runses = (10, 100)
    number_of_runses = (10,)
    nan_thresholds = tuple(range(1, 4))
    argument_sets = [
        {
            'research_sample': research_sample,
            'auxiliary_sample': auxiliary_sample,
            'auxiliary_missing': auxiliary_missing,
            'auxiliary_noise': auxiliary_noise,
            'payload_vars': payload_vars,
            'columns_to_keep': columns_to_keep,
            'number_of_runs': number_of_runs,
            'nan_threshold': nan_threshold
        } for research_sample, auxiliary_sample, auxiliary_missing,
              auxiliary_noise, payload_vars, columns_to_keep, number_of_runs,
              nan_threshold in
        itertools.product(research_samples, auxiliary_samples,
                          auxiliary_missings,
                          auxiliary_noises, payload_varses, columns_to_keeps,
                          number_of_runses, nan_thresholds)
    ]
    return argument_sets


def main(restart_index=0, restart_time=str(pd.to_datetime('today'))):

    argument_sets = get_all_arguments()

    # directory is this file's name, sans '.py', and a timestamp
    filename = os.path.splitext(os.path.basename(__file__))[0]

    sim_output_directory = os.path.join(
        definitions.OUTPUT_DIR,
        filename + restart_time.replace(' ', '_')
    )

    while True:
        try:
            os.mkdir(sim_output_directory)
        except PermissionError:
                response = input(f'The output directory could not be created '
                                 f'due to a permissions error. Please check '
                                 f'that you have write access to the directory '
                                 f'{definitions.OUTPUT_DIR}.\n\n'
                                 'Try again? [y/n] (default: y)')

                try_again = (not response) or (response.lower()[0] == 'y')
                if not try_again:
                    exit(1)
        except FileExistsError:
            break
        else:
            break


    data_location = os.path.join(definitions.DATA_DIR, 'ucop9_03_08.dta')
    munger = UC_Munger(data_location, chunksize=10**6)

    df = munger.data

    # keep only students who enrolled in UC system
    uc_enrolled = df[df['adm00'] == 1].copy()

    restart_point_filename = f'{str(pd.to_datetime("today")).replace(" ", "_")}.pickle'
    output_filename_base = os.path.join(sim_output_directory, filename)
    result_summaries = dict()
    num_argument_sets = len(argument_sets)

    try:

        for i, arguments in enumerate(argument_sets[restart_index:]):
            number_of_runs = arguments['number_of_runs']
            nan_threshold = arguments['nan_threshold']

            sim = Simulator(uc_enrolled, DatasetName.UC,
                            nan_threshold=nan_threshold,
                            parameters=arguments)

            logging.info(f'++++++ BEGINNING BATCH {i+restart_index} ({number_of_runs} RUNS) ++++++')
            results = [sim.run() for _ in range(number_of_runs)]
            logging.info(f'++++++ COMPLETED BATCH {i+restart_index} ++++++')

            result_summary = Simulator.generate_output(arguments, results)
            output_filename = output_filename_base + str(pd.to_datetime('today')).replace(' ', '_')

            with open(output_filename + '.out', 'w') as f:
                f.write(result_summary)

            result_summaries[hash(str(arguments))] = result_summary
            print(f'Completed {i+1} / {num_argument_sets} argument sets ({((i+1)/num_argument_sets) * 100:0.4f}%)')

    except KeyboardInterrupt:
        logging.info(f'!!!!!!!!! Interrupted at {str(pd.to_datetime("today"))} !!!!!!!!!')

        logging.info(dedent(f'''\
            To resume where the simulation left off, use 
                $ python {os.path.basename(__file__)} {restart_point_filename}
            '''))

        print(f'...Stopping after receiving keyboard interrupt. See the '
              f'file {os.path.join(definitions.LOG_DIR, "simulation.log")} for more details about restarting.')

    finally:
        output_filename = output_filename_base + 'result_summaries.pickle'

        with open(output_filename, 'wb') as f:
            pickle.dump(result_summaries, f)

        with open(os.path.join(definitions.OUTPUT_DIR, restart_point_filename), 'wb') as f:
            pickle.dump(i, f)


if __name__ == '__main__':
    if len(sys.argv) > 2:
        print("This script should be invoked with at most one argument. The "
              "argument is optional: it should be the absolute path to a file "
              "created by a previous run that was interrupted. You will see the "
              "name of the file in log/simulation.log. When no argument is "
              "provided, the simulation begins at the beginning.")
    if len(sys.argv) == 2:
        restart_point_filename = sys.argv[1]
        try:
            with open(os.path.join(definitions.OUTPUT_DIR, restart_point_filename), 'rb') as f:
                restart_point = pickle.load(f)
                logging.info(f'Successfully re-started from {restart_point_filename}')
        except OSError:
            print("Could not open pickled argument generator. Please check that "
                  "the absolute filename is provided, and that the file at the "
                  "given location exists.")
            exit(1)
        main(restart_point, os.path.splitext(restart_point_filename)[0])
    else:
        main()


