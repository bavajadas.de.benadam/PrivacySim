import os

ROOT_DIR = os.path.dirname(os.path.abspath(__file__))
DATA_DIR = os.path.join(ROOT_DIR, 'data')
LOG_DIR = os.path.join(ROOT_DIR, 'log')
OUTPUT_DIR = os.path.join(ROOT_DIR, 'output')
PACKAGE_DIR = os.path.join(ROOT_DIR, 'privacysim')
