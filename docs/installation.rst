============
Installation
============


Stable release
--------------

To install Simulation of Database Privacy Risk, run this command in your terminal:

.. code-block:: console

    $ pip install privacysim

This is the preferred method to install Simulation of Database Privacy Risk, as it will always install the most recent stable release.

If you don't have `pip`_ installed, this `Python installation guide`_ can guide
you through the process.

.. _pip: https://pip.pypa.io
.. _Python installation guide: http://docs.python-guide.org/en/latest/starting/installation/


From sources
------------

The sources for Simulation of Database Privacy Risk can be downloaded from the `Bitbucket repo`_.

You can either clone the public repository:

.. code-block:: console

    $ git clone git://bitbucket.com/dsidi/privacysim

Or download the `tarball`_:

.. code-block:: console

    $ curl  -OL https://bitbucket.com/dsidi/privacysim/downloads/

Once you have a copy of the source, you can install it with:

.. code-block:: console

    $ python setup.py install


.. _Bitbucket repo: https://bitbucket.com/dsidi/privacysim
.. _tarball: https://bitbucket.com/dsidi/privacysim/downloads/
