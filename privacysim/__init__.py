# -*- coding: utf-8 -*-

"""Top-level package for Simulation of Database Privacy Risk."""

__author__ = """David Sidi"""
__email__ = 'david@sidiprojects.us'
__version__ = '0.1.0'

_use_cache = False
_cache_results = False
