# -*- coding: utf-8 -*-

"""Console script for PrivacySim."""
import os
import signal
import sys
import click
from privacysim import simulator


def _signal_handler(sig, _):
    """Handles the interrupt signal."""
    if not sig == signal.SIGINT:
        exit(os.EX_IOERR)

    print('\nCaught interrupt signal. Exiting...')
    exit(os.EX_OK)


@click.command()
def main():
    signal.signal(signal.SIGINT, _signal_handler)
    while True:
        cmd = input('🙈🙉🙊 PrivacySim> ')
        try:
            l_paren = cmd.index('(')
            r_paren = cmd.index(')')
            method_name = cmd[:l_paren]
            parameters = cmd[l_paren+1:r_paren]  # all params as one string
            
            click.echo(f'Calling {method_name}...')
            simulation_method = getattr(simulator, method_name)
            simulation_method(parameters)
            
        except Exception as e:  # XXX: fixme. Add available commands to response
            click.echo(f'Command {cmd} not recognized. Try again...')
            click.echo(e)


if __name__ == "__main__":
    sys.exit(main())  # pragma: no cover
