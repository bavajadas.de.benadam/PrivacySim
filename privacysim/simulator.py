# -*- coding: utf-8 -*-
"""
This module contains tools for a pipeline simulating attack on the privacy of
released data.

author: david (david@sidiprojects.us)
"""
import random
from abc import ABC, abstractmethod
from textwrap import dedent

import observed_values
from utilities import proclaim, DatasetName

import time
import pprint
import definitions
import logging
import os
import pandas as pd
import numpy as np
import itertools as it
import configparser

# logging
log_file_path = os.path.join(definitions.LOG_DIR, 'simulation.log')
logging.basicConfig(filename=log_file_path, level=logging.INFO)


class Munger(ABC):
    """Munge the data; i.e., prepare it to be analyzed or otherwise used.

    :param str file_or_directory_path: The file (or directory location of files)
     to be munged. If a directory name, all files in the directory will be
     combined in pandas dataframes.
    """
    def __init__(self, file_or_directory_path, chunksize):
        self.has_cleaned_data = False
        self.file_or_directory_path = file_or_directory_path
        self.chunksize = chunksize
        self._data = None

    @property
    def data(self):
        if not self.has_cleaned_data:
            self._load_data()
            self._preprocess()
            self.has_cleaned_data = True
        return self._data

    def _preprocess(self):
        """Perform all preprocessing tasks."""
        self.clean()
        self.standardize()
        self.verify()

    @abstractmethod
    def clean(self):
        """Remove or replace unwanted characters, abbreviations, words."""
        pass

    @abstractmethod
    def standardize(self):
        """"""
        pass

    @abstractmethod
    def verify(self):
        pass

    @abstractmethod
    def _load_data(self):
        pass


class MIMIC_Munger(Munger):
    """Munge the MIMIC III data set."""
    def __init__(self, directory_path):
        super(MIMIC_Munger, self).__init__(directory_path)
        self._raw_data_chunks = None

    @proclaim('Cleaning data...', '...done cleaning.')
    def clean(self):
        pass

    @proclaim('Standardizing data...', '...done standardizing.')
    def standardize(self):
        pass

    @proclaim('Verifying data...', '...done verifying.')
    def verify(self):
        # TODO:
        #  ADMISSIONS: admit time < discharge time, admit time < death time,
        #               ADMISSION_TYPE=NEWBORN should never match with any
        #               MARITAL_STATUS but SINGLE
        pass

    def _load_data(self):
        """Load large data in chunks."""
        path = self.file_or_directory_path
        chunk_size = (47 * 10**6) / len(list(os.scandir(path)))
        self._raw_data_chunks = \
            [pd.read_csv(filepath_or_buffer=filepath_entry.name, chunksize=chunk_size)
                for filepath_entry in os.scandir(path)]


class UC_Munger(Munger):
    """Munge the UC Dataset"""
    def __init__(self, file_path, chunksize):
        super(UC_Munger, self).__init__(file_path, chunksize)
        self._raw_data = None

    @proclaim('Cleaning data...', '...done cleaning.')
    def clean(self):
        original_columns = ['year'] + \
                           [f'app0{i}' for i in range(10) if not i == 2] + \
                           [f'adm0{i}' for i in range(10) if not i == 2] + \
                           ['race', 'state'] + \
                           [f'sati_{area}' for area in ['verbal', 'math']] + \
                           ['satii_writ'] + \
                           [f'satii_3rd_{i}' for i in ['score', 'subject']] + \
                           [f'{version}_hsgpa' for version in ['raw', 'ucadj']] + \
                           ['num_of_ap'] + \
                           [f'parent_{prop}' for prop in ['educa_max', 'income']] + \
                           ['ca_api_rank'] + \
                           [f'rollovers0{i}' for i in range(1, 10) if not i == 2] + \
                           ['elc_path'] + \
                           [f'app_cipcat_0{i}' for i in range(1, 10) if not i == 2] + \
                           ['gpa_cum'] + \
                           ['grad_cipcat'] + \
                           ['gpa_1y'] + \
                           ['enr_campus'] + \
                           ['bachelor'] + \
                           ['ttd_qtrs00']
        self._data = self._raw_data[original_columns]

    @proclaim('Standardizing data...', '...done standardizing.')
    def standardize(self):
        self._data_chunks = self._raw_data  # no op
        pass

    @proclaim('Verifying data...', '...done verifying.')
    def verify(self):
        pass

    def _load_data(self):
        path = self.file_or_directory_path
        try:
            self._raw_data = pd.read_stata(path)
        except AttributeError:
            logging.error(dedent(f'''\
                    {pd.to_datetime('today')}: Data could not be read for 
                    Munging. Check that the name of the database file in
                    config.ini matches the actual filename, and verify
                    existence of the database file. 
            '''))
            raise AttributeError


class PUMS_Munger(Munger):
    """Munge the PUMS data set."""
    def __init__(self, directory_path):
        super(PUMS_Munger, self).__init__(directory_path)
        self._raw_data_chunks = None

    @proclaim('Cleaning data...', '...done cleaning.')
    def clean(self):
        pass

    @proclaim('Standardizing data...', '...done standardizing.')
    def standardize(self):
        pass

    @proclaim('Verifying data...', '...done verifying.')
    def verify(self):
        pass

    def _load_data(self):
        """Load large data in chunks."""
        path = self.file_or_directory_path
        chunk_size = self.chunksize
        self._raw_data_chunks = \
            [pd.read_csv(filepath_or_buffer=filepath_entry.name, sep=',', chunksize=chunk_size)
                for filepath_entry in os.scandir(path)]


class SimulationResult(object):
    """The result of a simulation run

    :param parameters: The set of parameters for the run
    :param iteration: Which iteration the result corresponds to
    :param num_of_iterations: The total number of iterations
    :param time_of_run: The time at which the result was obtained
    :param recall: The number of true positives out of all the positives
     actually in the data.
    :param int num_apparent_matches: How many unique matches were found in the
     sample, where some uniques were only apparent, having doubles outside the
     dataset.
    :param int num_true_matches: How many unique matches were not merely apparent
     in the data.
    :param int num_uniques: The number of unique records in the population (not
     just the sample).
    """
    def __init__(self,
                 parameters,
                 num_apparent_matches=0,
                 num_true_matches=0,
                 iteration=0,
                 num_iterations=0,
                 time_of_run=None,
                 num_release_uniques=0,
                 num_uniques=0):
        self.parameters = parameters
        self.iteration = iteration
        self.num_iterations = num_iterations
        self.time_of_run = time_of_run
        self.num_uniques = num_uniques
        self.num_release_uniques = num_release_uniques

        self.num_apparent_matches = num_apparent_matches
        self.num_true_matches = num_true_matches

    def __str__(self):
        return dedent(f'''\
            Simulation run {self.iteration} out of {self.num_iterations}, at {self.time_of_run}
               * Precision: {self.precision}
               * Recall: {self.recall}
               * Number of apparent matches: {self.num_apparent_matches}
               * Number of true matches: {self.num_true_matches}
            ''')

    @property
    def precision(self):
        """The number of true positive calls out of all the positive calls"""
        try:
            precision = self.num_true_matches / self.num_apparent_matches
        except ZeroDivisionError:
            precision = 0.0
            logging.info(f'Run {self.iteration} had no apparent matches, so its '
                          'precision is 0.0')
        return precision

    @property
    def recall(self):
        """The number of true positive calls out of the positives in the data."""
        try:
            recall = self.num_true_matches / self.num_uniques
        except ZeroDivisionError:
            recall = 0.0
            logging.info(f'Run {self.iteration} had no unique records in the '
                          'population. This will be true of all runs')
        return recall

    @property
    def release_recall(self):
        try:
            release_recall = self.num_true_matches / self.num_release_uniques
        except ZeroDivisionError:
            release_recall = 0.0
            logging.info(f'Run {self.iteration} had no unique records in the '
                         'release. This may change for other runs, which will '
                         'be sampled differently.')
        return release_recall


    @property
    def research_sample_proportion(self):
        return self.parameters['research_sample']

    @property
    def auxiliary_sample_proportion(self):
        return self.parameters['auxiliary_sample']

    @property
    def auxiliary_noise_percentage(self):
        return self.parameters['auxiliary_noise']

    @property
    def payload_variables(self):
        return self.parameters['payload_vars']

    @property
    def included_variables(self):
        return self.parameters['columns_to_keep']


class Simulator(object):
    """Simulate a linkage attack

    :param pd.DataFrame data: The data to be attacked
    :param DatasetName dataset_name: The name of the dataset to attack
    :param nan_threshold: The maximum number of NaNs allowed in a row before it
     is disqualified from matching
    :param iteration_number: The iteration to start from (useful for restarts)
    :param dict parameters: Parameters to use, overriding configuration file
    """
    def __init__(self,
                 data: pd.DataFrame,
                 dataset_name: DatasetName,
                 nan_threshold: int = 0,
                 iteration_number: int = 0,
                 parameters=None):

        logging.info(f'**** Simulation started at {time.asctime()} ****')

        if parameters:
            self.parameters = parameters
        else:
            self.parameters = self._get_config(dataset_name)

        self.cell_ranges = None

        original_variables = data.columns
        data['sim_id'] = data.index
        self.data = data

        self.vars_without_identifier = original_variables

        self.released_data = None
        self.auxiliary_data = None

        self.apparent_matches = None
        self.true_matches = None

        self.nan_threshold = nan_threshold

        self.iteration_number = iteration_number

        data_uniques = self.data[original_variables].drop_duplicates(keep=False)
        self.current_simulation_result = \
            SimulationResult(parameters,
                             num_iterations=parameters['number_of_runs'],
                             num_uniques=len(data_uniques))

        logging.info('Parameters for this run: \n'
                     f'{pprint.pformat(self.parameters)}')

    def run(self):
        """Run the simulation

        :return:
        """
        self.iteration_number += 1

        simulation_result = SimulationResult(**vars(self.current_simulation_result))
        simulation_result.iteration_number = self.iteration_number

        logging.info(f'Running iteration {self.iteration_number}...')

        self._build_auxiliary_data()
        self._build_release_data()

        simulation_result.num_release_uniques = len(self.released_data.drop_duplicates(
            subset=self.parameters['columns_to_keep'], keep=False))

        # Run matching attack
        self.find_apparent_matches()
        simulation_result.num_apparent_matches = len(self.apparent_matches)

        self.find_true_matches()
        simulation_result.num_true_matches = len(self.true_matches)

        return simulation_result

    def find_apparent_matches(self):
        """Find the apparent matches from the simulated matching attack

           A matching attack finds unique records in the released data that
           match records in the auxiliary data. The match is "apparent" because
           what is unique in the released data may not be in the full data,
           since the released data is sampled (and may be further transformed).

        :rtype: None
        """
        # do not include sim_id in dedup, which was added to cols to keep in released_data
        released_variables = set(self.parameters['columns_to_keep'])
        released_data_uniques = self.released_data.drop_duplicates(subset=released_variables, keep=False)

        aux_data = self.auxiliary_data
        auxiliary_data_no_nans = aux_data.dropna()
        merge_variables = list(aux_data.columns.intersection(released_variables))

        apparent_matches = pd.merge(
            released_data_uniques,
            auxiliary_data_no_nans,  # has no duplicates
            how='inner',
            on=merge_variables,
            suffixes=('_release', '_aux'))

        if self.nan_threshold:
            auxiliary_data_w_nans = aux_data[aux_data.isna().any(axis=1)]
            special_nan_matches = self._find_matches_on_non_nans(self.nan_threshold,
                                                                 auxiliary_data_w_nans,
                                                                 released_data_uniques)
            apparent_matches = apparent_matches.append(special_nan_matches)

        self.apparent_matches = apparent_matches

    def find_true_matches(self):
        """Find the matches to the population found from the simulated attack

         A subset of the apparent matches are the true matches: unique matches
         that are not duplicated in the full population database, outside of
         what was released.

        :rtype: None
        """
        apparent_matches = self.apparent_matches
        # Comparison to None needed here since DataFrames can't be evaluated
        # for truth
        if apparent_matches is None:
            logging.error('Missing apparent matches. Have you run '
                          'find_apparent_matches()?')
            raise AttributeError('Missing apparent matches')

        true_matches = apparent_matches[
            apparent_matches['sim_id_release'] == apparent_matches['sim_id_aux']
        ]

        self.true_matches = true_matches

    @staticmethod
    def generate_output(parameters, results):
        """Generate formatted output about the runs

           See docstrings in :class:`SimulationResult` for definitions of the
           values reported here.

        :param list[SimulationResult] results: The results of a batch of runs
        :param dict parameters: The fixed parameters for the batch of runs
        :return: The summary results of the batch of runs
        :rtype: str
        """
        apparent_matches_runs = list(result.num_apparent_matches for result in results)
        mean_apparent_matches_runs = np.mean(apparent_matches_runs)
        std_apparent_matches_runs = np.std(apparent_matches_runs)

        true_matches_runs = list(result.num_true_matches for result in results)
        mean_true_matches_runs = np.mean(true_matches_runs)
        std_true_matches_runs = np.std(true_matches_runs)

        example_run = str(results[random.randint(0, len(results)-1)])
        num_uniques = results[0].num_uniques  # same for all results

        precision_runs = list(result.precision for result in results)
        mean_precision_runs = np.mean(precision_runs)
        std_precision_runs = np.std(precision_runs)

        recall_runs = list(result.release_recall for result in results)
        mean_recall_runs = np.mean(recall_runs)
        std_recall_runs = np.std(recall_runs)

        total_recall_runs = list(result.recall for result in results)
        mean_total_recall_runs = np.mean(total_recall_runs)
        std_total_recall_runs = np.std(total_recall_runs)

        output_string = dedent('''
            =================================================================================
            =                   PRIVACYSIM SIMULATION SUMMARY                               =
            =================================================================================
            Simulation completed at {now}.
            
            
            Parameters
            ---------------------------------------------------------------------------------
            {parameters}
            
            
            Statistics 
            ---------------------------------------------------------------------------------
            * Total uniques in the full, pre-released data: {num_uniques}
            
            * Mean number of apparent matches: {mean_apparent:0.6f}
            * Standard deviation of apparent matches: {std_apparent:0.6f}
            
            * Mean number of true matches: {mean_true:0.6f}
            * Standard deviation of true matches: {std_true:0.6f}
            
            * Mean precision: {mean_precision:0.6f}
            * Standard deviation of precision: {std_precision:0.6f}
            
            * Mean recall: {mean_recall:0.6f}
            * Standard deviation of recall: {std_recall:0.6f}
            
            * Mean total recall: {mean_total_recall:0.6f}
            * Standard deviation of total recall: {std_total_recall:0.6f}
            
            
            Example Individual Run Result 
            ---------------------------------------------------------------------------------
            {example_run}
            ''').format(now=str(pd.to_datetime('today')),
                        parameters=pprint.pformat(parameters),
                        num_uniques=num_uniques,
                        mean_apparent=mean_apparent_matches_runs,
                        std_apparent=std_apparent_matches_runs,
                        mean_true=mean_true_matches_runs,
                        std_true=std_true_matches_runs,
                        mean_precision=mean_precision_runs,
                        std_precision=std_precision_runs,
                        mean_recall=mean_recall_runs,
                        std_recall=std_recall_runs,
                        mean_total_recall=mean_total_recall_runs,
                        std_total_recall=std_total_recall_runs,
                        example_run=example_run)
        return output_string

    ##################
    # Private Methods
    ##################

    def _build_auxiliary_data(self):
        """Create the auxiliary data

           Values in the auxiliary data are unique; they are assumed to have
           been gathered from identified data.

           The auxiliary data is created by first removing a set of variables
           that are the payload in the released data. Next, two kinds of
           problems with data quality are introduced: some proportion of the
           cells are suppressed (replaced with NaN), and some proportion of the
           cells are noised (replaced by a distinct value randomly chosen from
           the values found in the data for that column).

        :rtype: None
        """
        self._set_sampled_auxiliary_data()

        # for efficiency, we do this here rather than when finding apparent matches,
        # since cellwise operations are expensive
        self.auxiliary_data.drop_duplicates(subset=self.vars_without_identifier, inplace=True, keep=False)

        self._suppress_payload(self.parameters['payload_vars'])

        self._cellwise_operation(self.auxiliary_data,
                                 self.parameters['auxiliary_missing'],
                                 self._suppressor)

        self._cellwise_operation(self.auxiliary_data,
                                 self.parameters['auxiliary_noise'],
                                 self._noiser)

    def _build_release_data(self):
        """Create the data that will be released

           The data is sampled before it is released.

        :rtype: None
        """
        self.released_data = self.data[self.parameters['columns_to_keep']+['sim_id']]
        self._set_sampled_release_data()

    @staticmethod
    def _find_matches_on_non_nans(threshold, aux_data_with_nans, released_data_uniques):
        """Count a row as matched if it matches everywhere it isn't NaN, up to
           a threshold number of NaNs.

           An attacker with an aggressive profile might count as matches
           any row that matches on all variables that they know about (i.e., that
           aren't NaNs).

        :param int threshold: Number of NaNs allowed in a row before it is
         disqualified as a match.
        :param pd.DataFrame aux_data_with_nans: The auxiliary data records that
         contain at least one NaN.
        :return pd.DataFrame: The records that match on all non-NaN rows,
         excepting records with more than the threshold number of NaNs.
        """
        shared_cols = aux_data_with_nans.columns.intersection(released_data_uniques.columns).difference(['sim_id'])
        aux_nan_cols = aux_data_with_nans.columns[aux_data_with_nans.isna().any()]
        shared_nan_cols = aux_nan_cols.intersection(shared_cols)

        # n choose k for 1 <= k < threshold
        nan_cols_combinations = it.chain.from_iterable(
            it.combinations(shared_nan_cols, k) for k in range(1, threshold)
        )

        all_matches = pd.DataFrame(columns=released_data_uniques.columns)
        for nan_cols_combination in nan_cols_combinations:

            nan_cols_combination = set(nan_cols_combination)
            non_nan_cols = shared_cols.difference(nan_cols_combination)

            # all rows with NaNs for all and only this combination of columns
            df = aux_data_with_nans[
                    (aux_data_with_nans[nan_cols_combination].isna().all(axis=1)) &
                    (aux_data_with_nans[non_nan_cols].notna().all(axis=1))
            ]

            # append all that match on the non_nan rows
            matches = pd.merge(
                released_data_uniques,
                df,
                how='inner',
                on=list(non_nan_cols),
                suffixes=('_release', '_aux'),
            )

            # set values in matches outside the auxiliary data to the value in the released data
            release_only_cols_suffixed = list(filter(lambda col: col.endswith('_release'), matches.columns))
            release_only_cols_unsuffixed = list(map(lambda col: col[:col.rfind('_release')], release_only_cols_suffixed))
            matches[release_only_cols_unsuffixed] = matches[release_only_cols_suffixed]

            all_matches = all_matches.append(matches[all_matches.columns.union(['sim_id_release', 'sim_id_aux'])])

        return all_matches

    @staticmethod
    def _noiser(df, coordinates):
        """Add noise to a cell in place

           The noise value is guaranteed not to match the existing value, and
           is chosen randomly from the range of values present in the data.

        :param df: The data to which to apply noise
        :param tuple[int,int] coordinates:
        :rtype: pd.DataFrame
        """
        vars2vals = observed_values.vars2vals
        for row, col in coordinates:
            col_name = df.columns[col]
            col_range = vars2vals[col_name]

            if len(col_range) <= 1:
                logging.info(f'Variable {col_name} could not be noised, since it has '
                              'only one value in the data.')
                continue

            # be sure noised value is a change from old value
            while True:
                noised_value = random.choice(col_range)
                if noised_value != df.iat[row, col]:
                    break

            df.iat[row, col] = noised_value

    @staticmethod
    def _suppressor(df, coordinates):
        """Suppress individual cells in place

        :param df:
        :param tuple[int,int] coordinates:
        :return:
        :rtype: pd.DataFrame
        """
        for row, col in coordinates:
            df.iloc[row, col] = np.nan

    @staticmethod
    def _cellwise_operation(df, proportion_to_change, operation):
        """Perform an operation on a proportion of the cells in the data, in place

        :param pd.DataFrame df: The data to operate on
        :param float proportion_to_change:
        :param function operation: A function taking a DataFrame, row, and
         column and returning a value.
        :rtype: None
        """
        # subtract 1 from the column size to ensure we don't change the sim_id
        num_of_cells_to_change = int(df.shape[0] * (df.shape[1]-1) * proportion_to_change)

        all_coords = np.array(
            np.meshgrid(np.arange(df.shape[0]),
                           np.arange(df.shape[1]-1)))\
            .T.reshape(-1, 2)

        rng = np.random.default_rng()
        coordinates = rng.choice(
            all_coords,
            size=num_of_cells_to_change,
            replace=False
        )

        operation(df, coordinates)

    def _set_sampled_release_data(self):
        """Sample the data to get the data that is to be released

           Released data may contain repeat records.

           Repeated calls result in sampling with replacement.

        :rtype: None
        """
        proportion_to_sample = self.parameters['research_sample']
        self.released_data = self.released_data.sample(frac=float(proportion_to_sample))

    def _set_sampled_auxiliary_data(self):
        """Sample the data to simulate finding linked auxiliary data

           Repeated calls result in sampling with replacement.

        :rtype: None
        """
        proportion_to_sample = self.parameters['auxiliary_sample']
        self.auxiliary_data = self.data.sample(frac=float(proportion_to_sample))

    def _suppress_payload(self, payload):
        """Remove the passed payload variables from the auxiliary data

        :param set[str] payload: The columns to remove
        :rtype: None
        """
        self.auxiliary_data.drop(payload, axis=1, inplace=True)

    def _get_config(self, dataset):
        """Get parameters from the configuration file

        :param DatasetName dataset: The dataset whose parameters are to be
         retrieved.
        :rtype dict: The parameter values by name
        """
        dataset_name = dataset.name

        config_path = os.path.join(definitions.PACKAGE_DIR, 'config.ini')
        cfg = configparser.ConfigParser()
        success = cfg.read(config_path)

        if not success:
            logging.info('Creating configuration file config.ini')
            self._create_new_config()
            cfg.read(config_path)

        # Use the dataset-specific method to get the parameters; e.g.,
        # do _get_uc_config(cfg['UC']) for UC data
        dataset_cfg_method = getattr(self, f'_get_{dataset_name.lower()}_config')

        return dataset_cfg_method(cfg[dataset_name])

    @staticmethod
    def _get_uc_config(cfg):
        """Get the parameters for the UC dataset

        :param cfg: The UC section of the ini file.
        :return:
        """
        int_keys = ('number_of_runs',)  # note necessity of trailing ','
        float_keys = ('research_sample', 'auxiliary_sample',
                      'auxiliary_missing', 'auxiliary_noise')
        list_keys = ('payload_vars', 'columns_to_keep')

        for int_key in int_keys:
            dataset_cfg = {
                int_key: cfg.getint(int_key)  # gets key from DEFAULT as needed
            }

        for float_key in float_keys:
            dataset_cfg[float_key] = cfg.getfloat(float_key)

        for list_key in list_keys:

            # parse the list
            str_list = cfg.get(list_key)
            values = str_list\
                .replace('\n', '')\
                .replace(' ', '')\
                .split(',')
            dataset_cfg[list_key] = values

        return dataset_cfg

    @staticmethod
    def _create_new_config():
        config_contents = dedent('''\
        ##
        # Special default values section
        #
        # These values are used when a parameter value cannot be found another way.
        #
        [DEFAULT]

        # the proportion to sample. Restore value: 0.25
        research_sample: 0.25

        # the proportion to sample for the auxiliary data. Restore value: 0.1
        auxiliary_sample: 0.1

        # the proportion to redact from the auxiliary set. Restore value: 0.1
        auxiliary_missing: 0.1

        # the proportion of cells to which to apply noise. Restore value: 0.1
        auxiliary_noise: 0.1

        # the payload variables to suppress from the auxiliary columns. Restore
        #  value: 'sati_math', 'sati_verbal', 'satii_writ'
        payload_vars: 'sati_math', 'sati_verbal', 'satii_writ'

        # the columns to keep, ignoring all others. Restore value:
        #  'year', 'race', 'state', 'sati_verbal', 'sati_math', 'satii_writ',
        #  'ucadj_hsgpa', 'parent_income', 'ca_api_rank', 'gpa_cum', 'grad_cipcat',
        #  'enr_campus', 'bachelor', 'ttd_qtrs00'
        columns_to_keep: 'year', 'race', 'state', 'sati_verbal', 'sati_math',
            'satii_writ', 'ucadj_hsgpa', 'parent_income', 'ca_api_rank', 'gpa_cum',
            'grad_cipcat', 'enr_campus', 'bachelor', 'ttd_qtrs00'

        # the number of iterations of the simulation to do. Restore value: 100
        number_of_runs: 100



        ##
        # Parameters for the UC Dataset
        #
        # Add values here to override the defaults for the UC dataset.
        #
        [UC]


        ##
        # Parameters for the MIMIC III Dataset
        #
        # Add values here to override the defaults for the MIMIC III dataset.
        #
        [MIMIC]


        ##
        # Parameters for the PUMS Dataset
        #
        # Add values here to override the defaults for the PUMS dataset.
        #
        [PUMS]
        ''')
        try:
            config_filename = os.path.join(definitions.PACKAGE_DIR, 'config.ini')
            with open(config_filename, 'w') as f:
                f.write(config_contents)
        except IOError:
            logging.error('Failed to create a new config.ini file (IO Error).')
            raise IOError(
                f'The configuration file (config.ini) was not found. '
                'There was an IO error when trying to create a new '
                'configuration file. Double check that you are able '
                'to write to the directory {definitions.PACKAGE_DIR}.'
            )
        else:
            logging.info('Successfully created a new config.ini file.')

