# -*- coding: utf-8 -*-
# utilities.py
# -------
# The Counter class is from John DeNero and Dan Klein at UC Berkeley;
# everything else is by David Sidi.
#
import glob
import logging
import math
import os
import re
import shutil
from enum import Enum, auto
from functools import wraps
from os import scandir

import dill
import itertools
import numpy as np
import pandas as pd
import privacysim as ps
from decorator import contextmanager


def generate_csv_pair(data_dir):
    """Get the estimated, ground-truth csv's.

       Get all csv's in the directory of this file that have the suffix
        '_est' or '_gt'
    """
    _est_suffix = '_est'
    _gt_suffix = '_gt'
    print(data_dir)
    curr_dir_csvs = [x for x in os.listdir(data_dir) if
                     x.endswith('.csv')]  # TODO rm

    for filename in curr_dir_csvs:
        # get all filenames matching except for suffixed `_b3' / `_gt'
        filename, _ = os.path.splitext(filename)
        if filename.endswith(_est_suffix):
            b3_filename = filename + '.csv'
            gt_filename = b3_filename.replace(_est_suffix, _gt_suffix)
            yield (b3_filename, gt_filename)


def proclaim(start_msg=None, end_msg='...done'):
    def verbose_inner(fn):
        @wraps(fn)
        def func_wrapper(*args, **kwargs):
            if start_msg:
                print(start_msg)

            result = fn(*args, **kwargs)

            if end_msg:
                print(end_msg)

            return result
        return func_wrapper
    return verbose_inner


def memoized(file_path):
    """Decorator for using a version of a value cached in a file.

    Takes a path to a pickled datastructure, and returns it if it exists; o.w.
    it just calls the function.

    :param file_path: Where to find the cached result (if it exists).
    :return:
    """
    def file_cached_inner(fn):
        @wraps(fn)
        def func_wrapper(*args, **kwargs):
            if ps._use_cache:
                try:
                    result = get_cached(file_path)
                except IOError:
                    logging.error(f'Could not open cached file at {os.path.basename(file_path)}. '
                                  f'Regenerating...')
                    ps._use_cache = False
                    result = fn(*args, **kwargs)
                    if ps._cache_results:
                        send_to_cache(result, file_path)
            else:
                result = fn(*args, **kwargs)
                if ps._cache_results:
                    send_to_cache(result, file_path)
            return result
        return func_wrapper
    return file_cached_inner


def split_test_train(test_size, src_dir, test_dir, train_dir):
    """Splits the dataset into training and testing sets, with specified
       numbers of files in each. Since we use scandir to generate the list
       of the directory contents, the partitioning is done in a single
       method. Files are copied (rather than, say, building a list of
       names...) since this method is used to prepare for evaluation of a
       classifier which expects training and test data as files in separate
       directories.

    :param test_size:
    :param src_dir:
    :param train_dir:
    :param test_dir:
    """
    scandir_func = scandir.scandir(src_dir)
    count_iter = itertools.count()
    for i, sf in itertools.izip(count_iter, scandir_func):
        if i < test_size:
            shutil.copy(sf.path, os.path.join(test_dir, sf.name))
        else:
            shutil.copy(sf.path, os.path.join(train_dir, sf.name))
    if i < test_size:
        raise AttributeError(
            'The number of files {sz} to get exceeds the total number of '
            'files in the specified location {dir}.'.
                format(sz=test_size, dir=src_dir)
        )
    return

def checked_iterdir(size, directory):
    """Generator for filenames of a specified number of files from a
       directory, scaling to very large directories.

       Uses scandir to generate files from the directory scalably.
       Generates an AttributeError if size exceeds the number of files in
       the directory.

    :param size: Number of files to get.
    :param directory: Directory with the files to get.
    """
    scandir_func = scandir.scandir(directory)
    count_iter = itertools.count()
    for i, sf in itertools.izip(count_iter, scandir_func):
        yield (i, sf)
    if i < size:
        raise AttributeError(
            'The number of files {sz} to get exceeds the total number of '
            'files in the specified location {dir}.'.
                format(sz=size, dir=directory)
        )
    # scandir_func = scandir.scandir(directory)
    #
    # # unpythonic, to avoid use of enumerate with scandir (which defeats the
    # # purpose of scandir)
    # for i in xrange(size):
    #     try:
    #         yield next(scandir_func)
    #     except StopIteration:
    #         raise AttributeError(
    #             'The number of files {sz} to get exceeds the total number of '
    #             'files in the specified location {dir}.'.
    #                 format(sz=size, dir=directory)
    #         )
    return


def subsequence_search(target, source):
    """Find a target subsequence in a source list.

    :param list target: The list to search for
    :param list source: The list to search in
    """
    search_start = 0
    while True:
        # find the next place where the first element in the target is found
        try:
            start_idx = search_start + source[search_start:].index(target[0])
        except ValueError:
            return None

        end_idx = start_idx + len(target)
        if source[start_idx:end_idx] == target:
            return start_idx, end_idx
        else:
            search_start = start_idx + 1

# ____________________________________________________
#
#   compute various measures of quality
# ____________________________________________________
#

class Quality_measures:
    def __init__(self, tp, tn, fp, fn):
        self.tp = tp
        self.tn = tn
        self.fp = fp
        self.fn = fn

    def precision_recall(self):
        """ Compute the precision-recall.

        :return (float,float): the precision and recall, as a pair
        """
        tp = self.tp
        tn = self.tn  # @UnusedVariable
        fp = self.fp
        fn = self.fn

        # handle edge case
        if tp == 0:
            precision = 0
            recall = 0
        else:
            precision = float(tp) / (tp + fp)
            recall = float(tp) / (tp + fn)

        return precision, recall

    def true_negative(self):
        """ Compute the true negative.

        :return float: the true negative value
        """
        return float(self.tn) / (self.tn + self.fp)

    def f_value(self, precision, recall, beta=1):
        """ Compute the F-value.

        The F-value is a weighted average of precision and recall
        perfect = 1, worst = 0

        :param float precision: tp / (tp + fp)
        :param float recall: tp / (tp + fn)
        :param float beta: controls how much more you weight recall over precision
        :return float: the F-value
        """
        if precision != 0 and recall != 0:
            return (1 + beta ** 2) * (
                (precision * recall) / (precision + recall))
        else:
            return 0

    def matthews_correlation_coefficient(self):
        """ Compute matthews correlation coefficient.

        MCC is a measure of correlation between observation and prediction: a perfect
        value is 1, random guessing yields 0, and perfect disagreement -1.

        :return float: the MCC
        """
        tp = self.tp
        tn = self.tn
        fp = self.fp
        fn = self.fn

        n = sum([tp, tn, fp, fn])
        s = float(tp + fn) / n
        p = float(tp + fp) / n

        factor1 = tp + fp
        factor2 = tp + fn
        factor3 = tn + fp
        factor4 = tn + fn

        if min(factor1, factor2, factor3, factor4) == 0:
            denom = 1
        else:
            denom = math.sqrt(factor1 * factor2 * factor3 * factor4)

        return (tp * tn - fp * fn) / denom

    def accuracy(self):
        """ Compute the accuracy.
        :return float: the accuracy
        """
        return float(self.tp + self.tn) / (
            self.tp + self.fp + self.fn + self.tn)


def hamming_distance(seq_1, seq_2, normalizer=lambda x: x):
    """The hamming distance between two iterables.

       If one sequence is bigger than the other, then the absolute values of
       their difference in size is added to the distance.
    """
    size_diff = abs(len(seq_1) - len(seq_2))
    min_diffs = sum(1 for (element, other_element) in zip(seq_1, seq_2) if
                normalizer(element) != normalizer(other_element))
    return min_diffs + size_diff

# ______________________________________
#
#   various other useful structures
# ______________________________________
#

class Counter(dict):
    """
    A counter keeps track of counts for eval set of keys.

    The counter class is an extension of the standard python
    dictionary type.  It is specialized to have number values
    (integers or floats), and includes eval handful of additional
    functions to ease the task of counting data.  In particular,
    all keys are defaulted to have value 0.  Using eval dictionary:

    eval = {}
    print(eval['test'])

    would give an error, while the Counter class analogue:

    >>> eval = Counter()
    >>> print(eval['test'])
    0

    returns the default 0 value. Note that to reference eval key
    that you know is contained in the counter,
    you can still use the dictionary syntax:

    >>> eval = Counter()
    >>> eval['test'] = 2
    >>> print(eval['test'])
    2

    This is very useful for counting things without initializing their counts,
    see for example:

    >>> eval['blah'] += 1
    >>> print(eval['blah'])
    1

    The counter also includes additional functionality useful in implementing
    the classifiers for this assignment.  Two counters can be added,
    subtracted or multiplied together.  See below for details.  They can
    also be normalized and their total count and arg max can be extracted.
    """

    def __getitem__(self, idx):
        self.setdefault(idx, 0)
        return dict.__getitem__(self, idx)

    def incrementAll(self, keys, count):
        """
        Increments all elements of keys by the same count.

        >>> eval = Counter()
        >>> eval.incrementAll(['one','two', 'three'], 1)
        >>> eval['one']
        1
        >>> eval['two']
        1
        """
        for key in keys:
            self[key] += count

    def argMax(self):
        """
        Returns the key with the highest value.
        """
        if len(self.keys()) == 0: return None
        all_Items = self.items()
        values = [x[1] for x in all_Items]
        maxIndex = values.index(max(values))
        return all_Items[maxIndex][0]

    def sortedKeys(self):
        """
        Returns eval list of keys sorted by their values.  Keys
        with the highest values will appear first.

        >>> eval = Counter()
        >>> eval['first'] = -2
        >>> eval['second'] = 4
        >>> eval['third'] = 1
        >>> eval.sortedKeys()
        ['second', 'third', 'first']
        """
        sortedItems = self.items()
        compare = lambda x, y: np.sign(y[1] - x[1])
        sortedItems.sort(cmp=compare)
        return [x[0] for x in sortedItems]

    def totalCount(self):
        """
        Returns the sum of counts for all keys.
        """
        return sum(self.values())

    def normalize(self):
        """
        Edits the counter such that the total count of all
        keys sums to 1.  The ratio of counts for all keys
        will remain the same. Note that normalizing an empty
        Counter will result in an error.
        """
        total = float(self.totalCount())
        if total == 0: return
        for key in self.keys():
            self[key] = self[key] / total

    def divideAll(self, divisor):
        """
        Divides all counts by divisor
        """
        divisor = float(divisor)
        for key in self:
            self[key] /= divisor

    def copy(self):
        """
        Returns eval copy of the counter
        """
        return Counter(dict.copy(self))

    def __mul__(self, y):
        """
        Multiplying two counters gives the dot product of their vectors where
        each unique label is eval vector element.

        >>> eval = Counter()
        >>> b = Counter()
        >>> eval['first'] = -2
        >>> eval['second'] = 4
        >>> b['first'] = 3
        >>> b['second'] = 5
        >>> eval['third'] = 1.5
        >>> eval['fourth'] = 2.5
        >>> eval * b
        14
        """
        nSum = 0
        x = self
        if len(x) > len(y):
            x, y = y, x
        for key in x:
            if key not in y:
                continue
            nSum += x[key] * y[key]
        return nSum

    def __radd__(self, y):
        """
        Adding another counter to eval counter increments the current counter
        by the values stored in the second counter.

        >>> eval = Counter()
        >>> b = Counter()
        >>> eval['first'] = -2
        >>> eval['second'] = 4
        >>> b['first'] = 3
        >>> b['third'] = 1
        >>> eval += b
        >>> eval['first']
        1
        """
        for key, value in y.items():
            self[key] += value

    def __add__(self, y):
        """
        Adding two counters gives eval counter with the union of all keys and
        counts of the second added to counts of the first.

        >>> eval = Counter()
        >>> b = Counter()
        >>> eval['first'] = -2
        >>> eval['second'] = 4
        >>> b['first'] = 3
        >>> b['third'] = 1
        >>> (eval + b)['first']
        1
        """
        addend = Counter()
        for key in self:
            if key in y:
                addend[key] = self[key] + y[key]
            else:
                addend[key] = self[key]
        for key in y:
            if key in self:
                continue
            addend[key] = y[key]
        return addend

    def __sub__(self, y):
        """
        Subtracting eval counter from another gives eval counter with the union of all keys and
        counts of the second subtracted from counts of the first.

        >>> eval = Counter()
        >>> b = Counter()
        >>> eval['first'] = -2
        >>> eval['second'] = 4
        >>> b['first'] = 3
        >>> b['third'] = 1
        >>> (eval - b)['first']
        -5
        """
        addend = Counter()
        for key in self:
            if key in y:
                addend[key] = self[key] - y[key]
            else:
                addend[key] = self[key]
        for key in y:
            if key in self:
                continue
            addend[key] = -1 * y[key]
        return addend


class Outputter:
    def __init__(self, verbose=0):
        self.verbose = verbose

    def verbose_print(self, required_verbosity, msg):
        """ prints only if verbose is set
        :param msg: the message to be printed in verbose mode
        """
        if self.verbose >= required_verbosity:
            print('\nVERBOSE: ', msg)

    @staticmethod
    def error(msg):
        """ standardize errors.

        Print the passed message, and exit with code 1.
        """
        print('\n', msg)
        exit(1)

    def type2tuple(s):
        """Convert a comma-separated-string to a tuple"""
        return tuple(s.split(', '))

    def singleton_str2str(s):
        """Remove outer brackets and two apostrophes from a
        list-as-string"""
        return s[3:-3]

    def list_str2tuple(s):
        """For a list-as-string, remove outer brackets, split on comma.
        Then for each element, remove double apostrophes."""
        pat = re.compile(r"""
                             (?:\'\')
                                  (.*?)     # capture this group
                             (?:\'\')
                             """, re.VERBOSE)
        return tuple(pat.findall(s))


def send_to_cache(obj, fq_filename):
    """Sends a file to cache.

    :param obj:
    :param fq_filename:
    :return:
    """
    logging.info("Caching to file {file}...".
                 format(file=os.path.basename(fq_filename)))
    try:
        with open(fq_filename, 'wb') as f:
            dill.dump(obj, f)
    except IOError:
        logging.warn('Caching failed for {}'.format(fq_filename))
        raise


def get_cached(fq_filename):
    """Returns a cached version of a data structure saved in a file.

    :param fq_filename:
    """
    logging.info("Using cached file {file}...".
                 format(file=os.path.basename(fq_filename)))
    with open(fq_filename, 'rb') as f:
        unpickled = dill.load(f)
        return unpickled


class ParamSource(Enum):
    DEBUG = auto()
    PROMPT = auto()
    ALL_COMBINATIONS = auto()


class DatasetName(Enum):
    UC = auto()
    MIMIC = auto()
    PUMS = auto()


