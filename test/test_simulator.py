#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Tests for `PrivacySim` package."""
from unittest import mock
from privacysim.simulator import *


def test_run_boundary_cases():
    """Test when the sampling fractions are 0 and 1, without auxiliary missing
       or noise.
    """
    data_location = os.path.join(definitions.DATA_DIR, 'ucop9_03_08.dta')
    df = next(pd.read_stata(data_location, chunksize=10))
    columns_to_keep = ['year', 'race', 'state', 'ucadj_hsgpa', 'parent_income',
                       'ca_api_rank', 'gpa_cum', 'grad_cipcat', 'enr_campus',
                       'bachelor', 'ttd_qtrs00']
    df = df.fillna(0)

    num_uniques_in_data = len(df[columns_to_keep].drop_duplicates(keep=False))

    parameters_variations = [(
        {'research_sample': research_sample,
         'auxiliary_sample': auxiliary_sample,
         'auxiliary_missing': 0.0,
         'auxiliary_noise': 0.0,
         'payload_vars': [],
         'columns_to_keep': columns_to_keep,
         'number_of_runs': 1,
        }, expected_value)
        for research_sample, auxiliary_sample, expected_value in
        [(0.0, 0.0, 0), (1.0, 1.0, num_uniques_in_data)]
    ]

    for parameters, expected_answer in parameters_variations:
        sim = Simulator(df, DatasetName.UC, iteration_number=0,
                        parameters=parameters)
        sim.run()
        true_matches = sim.true_matches
        assert(len(true_matches) == expected_answer)


def test_integration():
    """Test using a simple dataset that the result is as expected."""
    columns_to_keep = ['year', 'race', 'state', 'sati_verbal', 'sati_math',
                       'satii_writ', 'ucadj_hsgpa', 'parent_income',
                       'ca_api_rank',  'gpa_cum', 'grad_cipcat', 'enr_campus',
                       'bachelor', 'ttd_qtrs00']

    # pre-sampled release data, including only columns to keep
    df = pd.DataFrame(
        [
           # year      race      state sati_verbal sati_math satii_writ ucadj_hsgpa  parent_income      ca_api_rank gpa_cum grad_cipcat  enr_campus     bachelor  ttd_qtrs00
           ["1998-2000", "White", "CA", "600-649", "600-649", "600-649", "3.50-3.99", "100,000 or Above", np.nan, "", "", np.nan, "", np.nan],
           ["1992-1994", "URM", "", "700-", "650-699", "200-449", "", "", np.nan, "", "", np.nan, "", np.nan],
           ["1992-1994", "Other", "", "700-", "700-", "650-699", "", "", np.nan, "", "", np.nan, "", np.nan],
           ["1995-1997", "White", "CA", "600-649", "650-699", "", "3.50-3.99", "100,000 or Above", np.nan, "", "", np.nan, "", np.nan],
           ["1995-1997", "Asian", "", "700-", "700-", "700-", "4.00-4.49", "20,000-29,999", np.nan, "", "", np.nan, "", np.nan],
           ["1992-1994", "Asian", "", "600-649", "600-649", "500-549", "3.50-3.99", "100,000 or Above", np.nan, "", "", np.nan, "", np.nan],
           ["1992-1994", "White", "NOT CA", "600-649", "550-599", "600-649", "0.00-3.49", "50,000-59,999", np.nan, "", "", np.nan, "", np.nan],
           ["1992-1994", "White", "NOT CA", "700-", "600-649", "650-699", "4.00-4.49", "100,000 or Above", np.nan, "", "", np.nan, "", np.nan],
           ["1992-1994", "Asian", "", "650-699", "650-699", "650-699", "4.00-4.49", "100,000 or Above", np.nan, "", "", np.nan, "", np.nan],
           ["1992-1994", "Other", "", "700-", "700-", "650-699", "", "", np.nan, "", "", np.nan, "", np.nan]
        ], columns=columns_to_keep
    )

    # release sampling fraction is 1/2
    released_data = pd.DataFrame(
        [
            ["1992-1994", "URM", "", "700-", "650-699", "200-449", "", "", np.nan, "", "", np.nan, "", np.nan],
            ["1992-1994", "Other", "", "700-", "700-", "650-699", "", "", np.nan, "", "", np.nan, "", np.nan],
            ["1995-1997", "White", "CA", "600-649", "650-699", "", "3.50-3.99", "100,000 or Above", np.nan, "", "", np.nan, "", np.nan],
            ["1998-2000", "White", "CA", "600-649", "600-649", "600-649", "3.50-3.99", "100,000 or Above", np.nan, "", "", np.nan, "", np.nan],
            ["1992-1994", "Asian", "", "650-699", "650-699", "650-699", "4.00-4.49", "100,000 or Above", np.nan, "", "", np.nan, "", np.nan]
        ], columns=columns_to_keep
    )
    released_data.loc[:, 'sim_id'] = [1, 2, 3, 0, 8]

    # auxiliary sampling fraction is 1/10
    auxiliary_sample = pd.DataFrame(
        [
            ["1995-1997", "White", "CA", "600-649", "650-699", "", "3.50-3.99", "100,000 or Above", np.nan, "", "", np.nan, "", np.nan, 3]
        ], columns=columns_to_keep + ['sim_id']
    )

    # remove payload
    payload = ['satii_writ', 'sati_verbal', 'sati_math']
    auxiliary_sample_no_payload = auxiliary_sample[auxiliary_sample.columns.difference(payload)]

    # (no noise added to auxiliary data, no missing)

    number_of_runs = 10
    nan_threshold = 4
    parameters = {'number_of_runs': number_of_runs,
                  'columns_to_keep': columns_to_keep}

    logging.basicConfig(level=logging.CRITICAL)

    sim = Simulator(data=df,
                    dataset_name=DatasetName.UC,
                    nan_threshold=nan_threshold,
                    iteration_number=0,
                    parameters=parameters)

    sim.released_data = released_data
    sim.auxiliary_data = auxiliary_sample_no_payload

    expected_apparent_matches = pd.DataFrame(
        [["1995-1997", "White", "CA", "600-649", "650-699", "", "3.50-3.99", "100,000 or Above", np.nan, "", "", np.nan, "", np.nan, 3, 3, 3]],
        columns=columns_to_keep + ['sim_id_release', 'sim_id_aux', 'sim_id'])
    sim.find_apparent_matches()
    pd.testing.assert_frame_equal(sim.apparent_matches, expected_apparent_matches, check_dtype=False)

    sim.find_true_matches()
    pd.testing.assert_frame_equal(sim.true_matches, expected_apparent_matches, check_dtype=False)

    sim.auxiliary_data = pd.DataFrame(
        [["1992-1994", "Other", "", "700-", "700-", "650-699", "", "", np.nan, "", "", np.nan, "", np.nan, 9]],
        columns=columns_to_keep + ['sim_id'])

    sim.apparent_matches = None

    expected_apparent_matches = pd.DataFrame(
        [["1992-1994", "Other", "", "700-", "700-", "650-699", "", "", np.nan, "", "", np.nan, "", np.nan, 2, 9, 2]],
        columns=columns_to_keep + ['sim_id_release', 'sim_id_aux', 'sim_id'])

    sim.find_apparent_matches()
    pd.testing.assert_frame_equal(sim.apparent_matches, expected_apparent_matches, check_dtype=False)
    sim.true_matches = None
    sim.find_true_matches()
    assert sim.true_matches.empty


@mock.patch('privacysim.simulator.Simulator._build_auxiliary_data')
@mock.patch('privacysim.simulator.Simulator._build_release_data')
@mock.patch('privacysim.simulator.Simulator.find_apparent_matches')
@mock.patch('privacysim.simulator.Simulator.find_true_matches')
@mock.patch('privacysim.simulator.SimulationResult')
@mock.patch('privacysim.simulator.len', return_value=0)
def test_run(*_):
    """Be sure that the run method runs all steps if the data is there"""
    mocksim = mock.Mock()
    mocksim.iteration_number = 0
    mocksim.parameters = mock.MagicMock()
    Simulator.run(mocksim)

    mocksim._build_auxiliary_data.assert_called_once()
    mocksim._build_release_data.assert_called_once()
    mocksim.find_apparent_matches.assert_called_once()
    mocksim.find_true_matches.assert_called_once()


@mock.patch('privacysim.simulator.SimulationResult')
@mock.patch('privacysim.simulator.len', return_value=0)
def test_run_updates_iterations(*_):
    """Runs should increment the iteration number."""
    mocksim = mock.Mock()
    mocksim.iteration_number = 0
    mocksim.parameters = mock.MagicMock()
    Simulator.run(mocksim)
    assert(mocksim.iteration_number == 1)


@mock.patch('privacysim.simulator.observed_values')
def test_sim_id_is_preserved(mock_observed_values):
    """Simulation ID variable should not be affected by noising/suppression."""
    df = pd.DataFrame([[1, 2, 3, 17],
                       [4, 5, 6, 9]],
                      columns=['a', 'b', 'c', 'sim_id'])
    mock_vars2vals = mock.MagicMock()
    mock_vars2vals.__getitem__.return_value = [99, 100]
    mock_observed_values.vars2vals = mock_vars2vals

    Simulator._cellwise_operation(df, 1.0, Simulator._noiser)  # mutates df

    assert(df.iloc[0, 3] == 17 and df.iloc[1, 3] == 9)


def test__find_matches_on_non_nans():
    """Check that the right matches are made on simple data with NaNs present"""

    released_data_uniques = pd.DataFrame([[1, 2, 3],
                                          [4, 5, 6],
                                          [7, 8, 9]],
                                         columns=['foo', 'bar', 'baz'])
    aux_data_with_nans = pd.DataFrame([[4, np.nan, np.nan],
                                       [7, 8, np.nan]],
                                      columns=released_data_uniques.columns)
    released_data_uniques.loc[:, 'sim_id'] = released_data_uniques.index
    aux_data_with_nans.loc[:, 'sim_id'] = aux_data_with_nans.index

    threshold = 3  # 3 or more NaNs disqualify a match
    actual_result = \
        Simulator._find_matches_on_non_nans(threshold, aux_data_with_nans, released_data_uniques)

    expected_result = pd.DataFrame([[4, 5, 6, 1], [7, 8, 9, 2]],
                                   columns=released_data_uniques.columns)

    intersection = pd.merge(actual_result, expected_result, how='inner')
    assert(len(intersection) == len(expected_result))

    threshold = 2  # 2 or more NaNs disqualify a match
    actual_result = \
        Simulator._find_matches_on_non_nans(threshold, aux_data_with_nans, released_data_uniques)
    expected_result = pd.DataFrame([[7, 8, 9, 2]],
                                   columns=released_data_uniques.columns)

    intersection = pd.merge(actual_result, expected_result, how='inner')
    assert(len(intersection) == len(expected_result))


def test_nan_arent_matched():
    """
    NaNs should disqualify a match, since they are values the attacker
    couldn't get
    """
    df = pd.DataFrame([[1, 2, np.nan],
                       [1, np.nan, 3],
                       [np.nan, 2, 3],
                       [1, 2, 3]],
                      columns=['a', 'b', 'c'])

    mocksim = mock.Mock()
    mock_current_simulation_result = mock.Mock(SimulationResult)
    mocksim.parameters = {'columns_to_keep': ['a', 'b', 'c']}
    mocksim.auxiliary_data = df
    mocksim.released_data = df
    mocksim.nan_threshold = 0
    mocksim.current_simulation_result = mock_current_simulation_result

    Simulator.find_apparent_matches(mocksim)
    expected_df = pd.DataFrame([[1, 2, 3]],  columns=['a', 'b', 'c'])
    result_df = mocksim.apparent_matches

    # note that equals() treats matching NaN values as equal, which would give
    # the wrong result if there were rows with NaNs in them that matched. eq()
    # does not have this problem
    assert(result_df.eq(expected_df).all().all())


def test_sampling_without_replacement():
    """Cellwise operation should sample cells without replacement."""
    df = pd.DataFrame([[1, 2, 3, 17],
                       [4, 5, 6, 9]],
                      columns=['a', 'b', 'c', 'sim_id'])

    def annihilator(df, coordinates):
        for row, col in coordinates:
            df.iat[row,col] = 0
        return df

    Simulator._cellwise_operation(df, 1.0, annihilator)  # mutates df
    expected_df = pd.DataFrame([[0, 0, 0, 17],
                                [0, 0, 0, 9]],
                               columns=df.columns)
    assert(expected_df.equals(df))


@mock.patch('privacysim.simulator.observed_values')
def test_suppressor_type_conversion(mock_observed_values):
    """Be sure that conversion to floats doesn't interfere with comparisons"""
    df = pd.DataFrame([[1, 2, 3, 17],
                       [4, 5, 6, 9]],
                      columns=['a', 'b', 'c', 'sim_id'])
    coordinates = [
        (0, 0),
        (0, 1),
        (1, 0),
        (1, 1)
    ]
    Simulator._suppressor(df, coordinates)  # mutates df

    # suppress all coordinates
    mock_vars2vals = mock.MagicMock()
    mock_vars2vals.__getitem__.return_value = [99, 100]
    mock_observed_values.vars2vals = mock_vars2vals

    # now noise the same coordinates without a problem
    Simulator._noiser(df, coordinates)  # mutates df
    for row, col in coordinates:
        assert(df.iloc[row, col] in {99,100})


@mock.patch('privacysim.simulator.Simulator._suppress_payload')
@mock.patch('privacysim.simulator.Simulator._cellwise_operation')
def test_auxiliary_data_has_no_duplicates(*_):
    mocksim = mock.Mock()
    mocksim.data = pd.DataFrame([[1, 2, 3, 17],
                                 [4, 5, 6, 9],
                                 [4, 5, 6, 10],
                                 [4, 5, 6, 11],
                                 [4, 5, 6, 12],
                                 [4, 5, 6, 13]],
                                columns=['a', 'b', 'c', 'sim_id'])

    mockparams = {'auxiliary_sample': 1.0,
                  'payload_vars': [],
                  'auxiliary_missing': 0.0,
                  'auxiliary_noise': 0.0}
    mocksim.parameters = mockparams
    mocksim.vars_without_identifier = ['a', 'b', 'c']
    Simulator._build_auxiliary_data(mocksim)

    expected_data = pd.DataFrame([[1, 2, 3, 17]], columns=['a', 'b', 'c', 'sim_id'])
    assert(mocksim.auxiliary_data.equals(expected_data))


def test_apparent_matches():
    mocksim = mock.Mock()

    mockparams = {'columns_to_keep': ['a', 'b', 'c']}
    mocksim.parameters = mockparams

    mocksim.released_data = pd.DataFrame([[1, 2, 3, 17],
                                          [4, 5, 6, 9],
                                          [10, 11, 12, 4],
                                          [10, 11, 12, 3],
                                          [7, 8, 9, 2],
                                          ],  columns=['a', 'b', 'c', 'sim_id'])
    mocksim.auxiliary_data = pd.DataFrame([[1, 2, 3, 'stuff', 19],  # <-- matches row unique in both, diff id
                                           [7, 8, 9, 'stuff', 2],   # <-- matches unique row in both, id matches
                                           [10, 11, 12, 'stuff', 3],  # <-- matches nonunique row in release
                                           ],  columns=['a', 'b', 'c', 'other_col', 'sim_id']
                                          )

    expected_apparent_matches = pd.DataFrame([[1, 2, 3, 17, 'stuff', 19],
                                              [7, 8, 9, 2, 'stuff', 2]],
                                             columns=['a', 'b', 'c', 'sim_id_x', 'other_col', 'sim_id_y'])
    mocksim.nan_threshold = 0

    Simulator.find_apparent_matches(mocksim)

    assert(mocksim.current_simulation_result.apparent_matches.equals(expected_apparent_matches))


def test_find_true_matches():
    apparent_matches = pd.DataFrame([[1, 1, 1, 0, 1],
                                     [2, 2, 2, 17, 17],
                                     [3, 3, 3, 8, 8]],
                                     columns=['a', 'b', 'c', 'sim_id_x', 'sim_id_y'])

    expected_true_matches = pd.DataFrame([[2, 2, 2, 17, 17],
                                          [3, 3, 3, 8, 8]],
                                          columns=['a', 'b', 'c', 'sim_id_x', 'sim_id_y'])

    mocksim = mock.Mock()
    mocksim.current_simulation_result.apparent_matches = apparent_matches

    assert(mocksim.current_simulation_result.true_matches.equals(expected_true_matches))


@mock.patch('privacysim.simulator.Simulator._set_sampled_release_data')
def test_build_release_data(_):
    mockparams = mock.MagicMock()
    mockparams.__getitem__.return_value = ['testing']
    mockdata = mock.MagicMock()
    mockdata.__getitem__.return_value = 'testing_value'
    mocksim = mock.Mock(Simulator)
    mocksim.parameters = mockparams
    mocksim.data = mockdata

    Simulator._build_release_data(mocksim)
    mockparams.__getitem__.assert_called_with('columns_to_keep')
    mockdata.__getitem__.assert_called_with(['testing', 'sim_id'])
    mocksim.released_data = 'testing_value'
    mocksim._set_sampled_release_data.assert_called_once()


@mock.patch('privacysim.simulator.float', return_value='testing')
def test_sampled_release_data(_):
    mockreleased_data = mock.Mock()
    mockreleased_data.sample.return_value = 'sample_testing'

    mockparameters = mock.MagicMock()
    mockparameters.__getitem__.return_value = 'impossible_float_value'

    mocksim = mock.Mock(['released_data', 'parameters'])

    mocksim.parameters = mockparameters
    mocksim.released_data = mockreleased_data

    Simulator._set_sampled_release_data(mocksim)

    mockparameters.__getitem__.assert_called_with('research_sample')
    mockreleased_data.sample.assert_called_with(frac='testing')
    assert mocksim.released_data == 'sample_testing'


def test_sampled_release_data_2():
    """Sampling the release data should produce data of the correct length"""
    df = pd.DataFrame([[1, 2],
                       [3, 4],
                       [3, 4],
                       [3, 4],
                       [3, 4],
                       [3, 4],
                       [3, 4],
                       [5, 6]])
    sample_fraction = 1/4
    sampled_df_length = 2

    mocksim = mock.Mock(Simulator)
    mocksim.released_data = df
    mocksim.parameters = {'research_sample': str(sample_fraction)}

    Simulator._set_sampled_release_data(mocksim)

    # note: mistakenly returning an empty DataFrame will pass the second
    # assertion, but fail the first
    assert len(mocksim.released_data) == sampled_df_length
    assert mocksim.released_data.isin(df).all().all()


def test_sampled_auxiliary_data_no_duplicates():
    """Auxiliary data should be created by sampling from the data.

       The data has no duplicates in this test.

    """
    df = pd.DataFrame([[1, 2],
                       [3, 4],
                       [5, 6],
                       [7, 8]])
    sample_fraction = 1/4
    sampled_df_length = 1

    mocksim = mock.Mock(Simulator)
    mocksim.vars_without_identifier = pd.RangeIndex(start=0, stop=1, step=1)
    mocksim.data = df
    mocksim.parameters = {'auxiliary_sample': str(sample_fraction)}

    Simulator._set_sampled_auxiliary_data(mocksim)

    # note: mistakenly returning an empty DataFrame will pass the second
    # assertion, but fail the first
    assert len(mocksim.auxiliary_data) == sampled_df_length
    assert mocksim.auxiliary_data.isin(df).all().all()


def test_sample():
    df = pd.DataFrame([[1, 2],
                       [3, 4],
                       [5, 6]])
    full_size = len(df)

    sample_proportions = (i/3 for i in range(0,4))
    for sample_proportion in sample_proportions:
        sampled_df = df.sample(frac=sample_proportion, axis=0)
        sample_size = len(sampled_df)
        assert sample_size == sample_proportion * full_size


@mock.patch('privacysim.simulator.logging')
@mock.patch('privacysim.simulator.DatasetName')
@mock.patch('privacysim.simulator.configparser.ConfigParser')
def test_create_config_file(MockConfig, mock_dn, mock_logging):
    """Should create a new configuration file, and read it in, when an
       existing configuration file can't be read.
    """
    # config file can't be read
    MockConfig.return_value.read.return_value = False

    # mock lookups in ConfigParser object
    MockConfig.return_value.__getitem__.return_value = 'testing'

    # mock the Dataset name lookup
    mock_dn.name = 'testing'

    # mock calls to create the new configuration file (_get_testing_config
    # doesn't exist; it's looked up based on the Dataset name, which we mock as
    # testing)
    attrs = {'_create_new_config.return_value': True,
             '_get_testing_config.return_value': True}
    Mocksim = mock.Mock(**attrs)

    Simulator._get_config(Mocksim, mock_dn)

    assert(MockConfig.return_value.read.call_count == 2)
    Mocksim._create_new_config.assert_called_once()
    mock_logging.info.assert_called_once()


@mock.patch('privacysim.simulator.configparser.ConfigParser')
def test_get_num_runs_uc_config(MockConfig):
    """For the UC data, should get the number of runs"""

    for num_runs in (n/2 for n in range(-1, 20000)):

        # Testing that the value from getint() is used to get the number of
        # runs
        MockConfig.return_value.getint.return_value = num_runs
        MockConfig.return_value.getfloat.return_value = 0.0
        MockConfig.return_value.get.return_value = 'foo, bar'

        cfg = Simulator._get_uc_config(MockConfig())

        assert(cfg['number_of_runs'] == num_runs)


@mock.patch('privacysim.simulator.configparser.ConfigParser')
def test_get_sample_percentages_uc_config(MockConfig):

    for sample_percentage in (1/n for n in range(1, 1000)):
        MockConfig.return_value.getfloat.return_value = sample_percentage
        MockConfig.return_value.getint.return_value = 1
        MockConfig.return_value.get.return_value = 'foo, bar'

        cfg = Simulator._get_uc_config(MockConfig())

        assert(cfg['research_sample'] == sample_percentage)
        assert(cfg['auxiliary_sample'] == sample_percentage)


@mock.patch('privacysim.simulator.configparser.ConfigParser')
def test_get_columns_to_keep_or_drop_uc_config(MockConfig):
    payload_vars = 'foo, bar, baz'
    payload_vars_list = ['foo', 'bar', 'baz']
    columns_to_keep = 'quux, quuz, zap'
    columns_to_keep_list = ['quux', 'quuz', 'zap']

    MockConfig.return_value.getfloat.return_value = 0.1
    MockConfig.return_value.getint.return_value = 1
    MockConfig.return_value.get.return_value = payload_vars

    cfg = Simulator._get_uc_config(MockConfig())

    assert(cfg['payload_vars'] == payload_vars_list)

    # reset the return value
    MockConfig.return_value.get.return_value = columns_to_keep
    cfg = Simulator._get_uc_config(MockConfig())

    assert(cfg['columns_to_keep'] == columns_to_keep_list)

